package net.ciderpunk.shooter.ents.powerup;

import com.badlogic.gdx.math.Vector2;

import net.ciderpunk.gamebase.gui.GameScreen;
import net.ciderpunk.shooter.ents.Player;
import net.ciderpunk.shooter.gui.MinimalismGame;

public class RedPower extends Powerup {

	public RedPower(MinimalismGame owner, Vector2 loc) {
		super(owner, loc);
		this.setAnim(Powerup.red);
	}

	public RedPower() {
		super();
	}

	@Override
	void doPowerup(Player player) {
		//give the dude some points!
		this.getOwner().getPlayer().increaseGun();
		this.getOwner().getPlayer().addScore(100);
	}

}
