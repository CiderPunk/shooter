package net.ciderpunk.shooter;

import net.ciderpunk.shooter.gui.MinimalismGame;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class ShooterGame extends ApplicationAdapter {
	SpriteBatch batch;
	Texture img;
	
	private MinimalismGame minGame;

	@Override
	public void create() {		
		minGame = new MinimalismGame(Constants.WorldWidth, Constants.WorldHeight);
	}

	
	@Override
	public void dispose() {
		minGame.dispose();
	}

	@Override
	public void render(){		
		minGame.update( Gdx.graphics.getDeltaTime() );
	}

	@Override
	public void resize(int width, int height) {
		minGame.resize(width, height);
	}

	@Override
	public void pause() {
		minGame.pauseGame();
	}

	@Override
	public void resume() {
		
	}
}
