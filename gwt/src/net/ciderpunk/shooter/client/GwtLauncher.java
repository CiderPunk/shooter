package net.ciderpunk.shooter.client;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.gwt.GwtApplication;
import com.badlogic.gdx.backends.gwt.GwtApplicationConfiguration;

import net.ciderpunk.shooter.Constants;
import net.ciderpunk.shooter.ShooterGame;

public class GwtLauncher extends GwtApplication {

        @Override
        public GwtApplicationConfiguration getConfig () {
          return new GwtApplicationConfiguration(Constants.WorldWidth, Constants.WorldHeight);
     
        }

        @Override
        public ApplicationListener getApplicationListener () {
                return new ShooterGame();
        }
}