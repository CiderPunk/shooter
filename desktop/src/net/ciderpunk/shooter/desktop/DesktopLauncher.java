package net.ciderpunk.shooter.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

import net.ciderpunk.shooter.Constants;
import net.ciderpunk.shooter.ShooterGame;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = Constants.WorldWidth;
		config.height = Constants.WorldHeight;
		new LwjglApplication(new ShooterGame(), config);
	}
}
